import { createRouter, createWebHistory } from "vue-router"
const routes = [
    {
        path:"/",
        name:"Login",
        component: ()=>import("../components/Login.vue")
    },
    {
        path:"/Index",
        name:"index",
        component: ()=>import("../components/Index.vue"),
        children:[
            {
                path:"/Home",
                name:"Home",
                meta:{
                    title:'首页'
                },
                component: ()=>import("../components/Home.vue")
            },
            {
                path:"/Admin",
                name:"Admin",
                meta:{
                    title:'管理员管理'
                },
                component: ()=>import("../components/admin/AdminManage.vue")
            },
            {
                path:"/User",
                name:"User",
                meta:{
                    title:'用户管理'
                },
                component: ()=>import("../components/user/UserManage.vue")
            },
            {
                path:"/Storage",
                name:"Storage",
                meta:{
                    title:'仓库管理'
                },
                component: ()=>import("../components/storage/StorageManage.vue")
            },
            {
                path:"/Goodstype",
                name:"Goodstype",
                meta:{
                    title:'物品分类管理'
                },
                component: ()=>import("../components/goodstype/GoodstypeManage.vue")
            },
            {
                path:"/Goods",
                name:"Goods",
                meta:{
                    title:'物品管理'
                },
                component: ()=>import("../components/goods/GoodsManage.vue")
            },
            {
                path:"/Record",
                name:"Record",
                meta:{
                    title:'记录管理'
                },
                component: ()=>import("../components/record/RecordManage.vue")
            }
        ]
    },

];
console.log(routes)
export function resetRouter() {
        router = createRouter({
        history: createWebHistory(),
        routes
    })
}

let router = createRouter({
    history: createWebHistory(),
    routes
})

const VueRouterPush = createRouter.prototype.push
createRouter.prototype.push = function push(to) {
    return VueRouterPush.call(this, to).catch(err => err)
}


export default router;