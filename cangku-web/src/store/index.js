import Vuex from 'vuex'
import router, {resetRouter} from "../router";
import createPersistedState from 'vuex-persistedstate';

function addNewRoute(menuList) {
    let routes = [...router.options.routes]
        if (routes.path === '/Index') {
            routes.forEach(routeItem => {
            JSON.parse(JSON.stringify(menuList)).forEach(menu => {
                const com =  "../components/"+ menu.menucomponent
                console.log(com)
                const childrenRoute = {
                    path: menu.menuclick,
                    name: menu.menuname,
                    meta: {
                        title: menu.menuname
                    },
                    // `../components/${menu.menucomponent}`
                     component: ()=> import("../components/admin/AdminManage.vue")
                }
                console.log(childrenRoute)
                routeItem.children.put(childrenRoute)
            })
        // })
    })
        }
    resetRouter()
    router.addRoute(routes)
}

export default new Vuex.Store({
    state: {
        menu: []
    },
    mutations: {
        setMenu(state, menuList) {
            state.menu = menuList

            // addNewRoute(menuList)
        }
    },
    getters: {
        getMenu(state) {
            return state.menu
        }
    },
    // plugins: [createPersistedState()]
})