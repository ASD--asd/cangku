package com.example.cangku.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.cangku.common.QueryPageParam;
import com.example.cangku.common.Result;
import com.example.cangku.entity.MenuEntity;
import com.example.cangku.entity.UserEntity;
import com.example.cangku.service.MenuService;
import com.example.cangku.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author li
 * @since 2024-05-24
 */
@RestController
@RequestMapping("/user-entity")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private MenuService menuService;


    @GetMapping("/list")
    public List<UserEntity> list(){
        return userService.list();
    }

    @GetMapping("/findByNo")
    public Result findByNo(@RequestParam String no){
        List list = userService.lambdaQuery().eq(UserEntity::getNo,no).list();
        return list.size()>0?Result.suc(list):Result.fail();
    }

    //新增
    @PostMapping("/save")
    public Result save(@RequestBody UserEntity userEntity){
        return userService.save(userEntity)?Result.suc():Result.fail();
    }
    //更新
    @PostMapping("/update")
    public Result update(@RequestBody UserEntity userEntity){
        return userService.updateById(userEntity)?Result.suc():Result.fail();
    }
    //删除
    @GetMapping ("/del")
    public Result del(@RequestParam String id){
        return userService.removeById(id)?Result.suc():Result.fail();
    }
    //登录
    @PostMapping("/login")
    public Result login(@RequestBody UserEntity userEntity){
        List list = userService.lambdaQuery()
                .eq(UserEntity::getNo,userEntity.getNo())
                .eq(UserEntity::getPassword,userEntity.getPassword()).list();

        if(list.size()>0){
            UserEntity user1 = (UserEntity)list.get(0);
            List menuList = menuService.lambdaQuery().like(MenuEntity::getMenuright,user1.getRoleId()).list();
            HashMap res = new HashMap();
            res.put("user",user1);
            res.put("menu",menuList);
            return Result.suc(res);
        }
        return Result.fail();
    }
    //修改
    @PostMapping("/mod")
    public boolean mod(@RequestBody UserEntity userEntity){
        return userService.updateById(userEntity);
    }
    //新增或修改
    @PostMapping("/saveOrMod")
    public boolean saveOrMod(@RequestBody UserEntity userEntity){
        return userService.saveOrUpdate(userEntity);
    }
    //删除
    @GetMapping("/delete")
    public boolean delete(Integer id){
        return userService.removeById(id);
    }
    //查询(模糊.匹配)
    @PostMapping("/listP")
    public Result listP(@RequestBody UserEntity userEntity){
        LambdaQueryWrapper<UserEntity> lambdaQueryWrapper = new LambdaQueryWrapper();
        if(StringUtils.isNotBlank(userEntity.getName())){
            lambdaQueryWrapper.like(UserEntity::getName,userEntity.getName());
        }
        return Result.suc(userService.list(lambdaQueryWrapper));
    }

    @PostMapping("/listPage")
    public List<UserEntity> listP(@RequestBody QueryPageParam query){

        HashMap param = query.getParam();
        String name = (String)param.get("name");
        System.out.println("name==="+(String)param.get("name"));

        Page<UserEntity> page = new Page();
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<UserEntity> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.like(UserEntity::getName,name);

        IPage result = userService.page(page,lambdaQueryWrapper);

        System.out.println("total=="+result.getTotal());

        return result.getRecords();
    }

    @PostMapping("/listPageC")
    public List<UserEntity> listPC(@RequestBody QueryPageParam query){
        HashMap param = query.getParam();
        String name = (String)param.get("name");
        System.out.println("name==="+name);

        Page<UserEntity> page = new Page();
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<UserEntity> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.like(UserEntity::getName,name);

        IPage result = userService.pageC(page);

//        IPage result = userService.pageCC(page,lambdaQueryWrapper);
        System.out.println("total=="+result.getTotal());

        return result.getRecords();
    }

    @PostMapping("/listPageCC")
    public Result listPCC(@RequestBody QueryPageParam query){
        HashMap param = query.getParam();
        String name = (String)param.get("name");
        String sex = (String)param.get("sex");
        String roleId = (String)param.get("roleId") ;
        System.out.println("name==="+name);

        Page<UserEntity> page = new Page();
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<UserEntity> lambdaQueryWrapper = new LambdaQueryWrapper();
        if(StringUtils.isNotBlank(name) && !"null".equals(name)) {
            lambdaQueryWrapper.like(UserEntity::getName, name);
        }
        if(StringUtils.isNotBlank(sex)) {
            lambdaQueryWrapper.eq(UserEntity::getSex,sex);
        }
        if(StringUtils.isNotBlank(roleId)) {
            lambdaQueryWrapper.eq(UserEntity::getRoleId,roleId);
        }
        IPage result = userService.pageCC(page,lambdaQueryWrapper);

        System.out.println("total=="+result.getTotal());

        return Result.suc(result.getRecords(),result.getTotal());
    }
}
