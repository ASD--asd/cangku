package com.example.cangku.controller;

import com.example.cangku.common.Result;
import com.example.cangku.entity.MenuEntity;
import com.example.cangku.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lihusheng
 * @since 2024-05-31
 */
@RestController
@RequestMapping("/menu-entity")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @GetMapping("/list")
    public Result list(@RequestParam String roleId){
        List list = menuService.lambdaQuery().like(MenuEntity::getMenuright,roleId).list();
        return Result.suc(list);
    }
}
