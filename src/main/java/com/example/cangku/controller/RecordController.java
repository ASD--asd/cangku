package com.example.cangku.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.cangku.common.QueryPageParam;
import com.example.cangku.common.Result;
import com.example.cangku.entity.GoodsEntity;
import com.example.cangku.entity.RecordEntity;
import com.example.cangku.service.GoodsService;
import com.example.cangku.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-02
 */
@RestController
@RequestMapping("/record-entity")
public class RecordController {
    @Autowired
    private RecordService recordService;
    @Autowired
    private GoodsService goodsService;
    //新增
    @PostMapping("/save")
    public Result save(@RequestBody RecordEntity recordEntity){
        GoodsEntity goods = goodsService.getById(recordEntity.getGoods());
        int n = recordEntity.getCount();
        //出库
        if ("2".equals(recordEntity.getAction())) {
            n = -n;
            recordEntity.setCount(n);
        }

        int num = goods.getCount() + n;
        goods.setCount(num);
        goodsService.updateById(goods);
        return recordService.save(recordEntity)?Result.suc():Result.fail();
    }
    //更新
    @PostMapping("/update")
    public Result update(@RequestBody RecordEntity recordEntity){
        return recordService.updateById(recordEntity)?Result.suc():Result.fail();
    }
    //删除
    @GetMapping("/del")
    public Result del(@RequestParam String id){
        return recordService.removeById(id)?Result.suc():Result.fail();
    }

    @PostMapping("/listPageCC")
    public Result listPCC(@RequestBody QueryPageParam query){
        HashMap param = query.getParam();
        String name = (String)param.get("name");
        String goodstype = (String) param.get("goodstype");
        String storage = (String) param.get("storage");
        String roleId = (String) param.get("roleId");
        String userid = (String) param.get("userid");
        System.out.println("name==="+name);

        Page<RecordEntity> page = new Page();
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        QueryWrapper<RecordEntity> QueryWrapper = new QueryWrapper();
        QueryWrapper.apply(" a.goods=b.id and b.storage=c.id and b.goodsType=d.id");
        if ("2".equals(roleId)) {
            // queryWrapper.eq(Record::getUserid,userId);
            QueryWrapper.apply(" a.userId= " + userid);
        }

        if(StringUtils.isNotBlank(name) && !"null".equals(name)) {
            QueryWrapper.like("b.name", name);
        }
        if (StringUtils.isNotBlank(goodstype) && !"null".equals(goodstype)) {
            QueryWrapper.eq("d.id", goodstype);
        }
        if (StringUtils.isNotBlank(storage) && !"null".equals(storage)) {
            QueryWrapper.eq("c.id", storage);
        }

        IPage result = recordService.pageCC(page,QueryWrapper);
        System.out.println("total=="+result.getTotal());

        return Result.suc(result.getRecords(),result.getTotal());
    }

    @GetMapping("/list")
    public Result list(){
        List list = recordService.list();
        return Result.suc(list);
    }
}
