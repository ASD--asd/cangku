package com.example.cangku.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.cangku.entity.MenuEntity;
import com.example.cangku.mapper.MenuMapper;
import com.example.cangku.service.MenuService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lihusheng
 * @since 2024-05-31
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuEntity> implements MenuService {
    @Resource
    private MenuMapper menuMapper;
}
