package com.example.cangku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.cangku.entity.GoodsEntity;
import com.example.cangku.mapper.GoodsMapper;
import com.example.cangku.service.GoodsService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-01
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, GoodsEntity> implements GoodsService {
    @Resource
    private GoodsMapper goodsMapper;
    @Override
    public IPage pageCC(IPage<GoodsEntity> page, LambdaQueryWrapper<GoodsEntity> wrapper){
        return goodsMapper.pageCC(page,wrapper);
    }
}
