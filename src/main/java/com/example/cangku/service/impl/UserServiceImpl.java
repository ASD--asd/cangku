package com.example.cangku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.cangku.entity.UserEntity;
import com.example.cangku.mapper.UserMapper;
import com.example.cangku.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author li
 * @since 2024-05-24
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Resource
    private UserMapper userMapper;
    @Override
    public IPage pageC(IPage<UserEntity> page){
        return userMapper.pageC(page);
    }

    @Override
    public IPage pageCC(IPage<UserEntity> page,LambdaQueryWrapper<UserEntity> wrapper){
        return userMapper.pageCC(page,wrapper);
    }
}
