package com.example.cangku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.cangku.entity.StorageEntity;
import com.example.cangku.mapper.StorageMapper;
import com.example.cangku.service.StorageService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-01
 */
@Service
public class StorageServiceImpl extends ServiceImpl<StorageMapper, StorageEntity> implements StorageService {
    @Resource
    private StorageMapper storageMapper;
    @Override
    public IPage pageCC(IPage<StorageEntity> page, LambdaQueryWrapper<StorageEntity> wrapper){
        return storageMapper.pageCC(page,wrapper);
    }
}
