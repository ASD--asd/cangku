package com.example.cangku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.cangku.entity.RecordEntity;
import com.example.cangku.mapper.RecordMapper;
import com.example.cangku.service.RecordService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-02
 */
@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, RecordEntity> implements RecordService {
    @Resource
    private RecordMapper recordMapper;
    @Override
    public IPage pageCC(IPage<RecordEntity> page, QueryWrapper<RecordEntity> wrapper){
        return recordMapper.pageCC(page,wrapper);
    }


}
