package com.example.cangku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.cangku.entity.GoodstypeEntity;
import com.example.cangku.mapper.GoodstypeMapper;
import com.example.cangku.service.GoodstypeService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-01
 */
@Service
public class GoodstypeServiceImpl extends ServiceImpl<GoodstypeMapper, GoodstypeEntity> implements GoodstypeService {
    @Resource
    private GoodstypeMapper goodstypeMapper;
    @Override
    public IPage pageCC(IPage<GoodstypeEntity> page, LambdaQueryWrapper<GoodstypeEntity> wrapper){
        return goodstypeMapper.pageCC(page,wrapper);
    }
}
