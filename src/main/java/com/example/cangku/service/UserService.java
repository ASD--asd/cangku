package com.example.cangku.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.cangku.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author li
 * @since 2024-05-24
 */
public interface UserService extends IService<UserEntity> {

    IPage pageC(IPage<UserEntity> page);


    IPage pageCC(IPage<UserEntity> page, @Param("ew") LambdaQueryWrapper<UserEntity> wrapper);
}
