package com.example.cangku.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.cangku.entity.StorageEntity;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-01
 */
public interface StorageService extends IService<StorageEntity> {

    IPage pageCC(IPage<StorageEntity> page, @Param("ew") LambdaQueryWrapper<StorageEntity> wrapper);
}
