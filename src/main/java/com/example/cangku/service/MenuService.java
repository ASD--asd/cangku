package com.example.cangku.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.cangku.entity.MenuEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lihusheng
 * @since 2024-05-31
 */
public interface MenuService extends IService<MenuEntity> {

}
