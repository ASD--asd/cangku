package com.example.cangku.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.cangku.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author li
 * @since 2024-05-24
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

    IPage pageC(IPage<UserEntity> page);


    IPage pageCC(IPage<UserEntity> page, @Param("ew") LambdaQueryWrapper<UserEntity> wrapper);
}
