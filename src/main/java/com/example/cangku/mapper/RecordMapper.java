package com.example.cangku.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.cangku.entity.RecordEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-02
 */
@Mapper
public interface RecordMapper extends BaseMapper<RecordEntity> {
    IPage pageCC(IPage<RecordEntity> page, @Param("ew") QueryWrapper<RecordEntity> wrapper);

}
