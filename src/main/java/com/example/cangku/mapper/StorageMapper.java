package com.example.cangku.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.cangku.entity.StorageEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-01
 */
@Mapper
public interface StorageMapper extends BaseMapper<StorageEntity> {
    IPage pageCC(IPage<StorageEntity> page, @Param("ew") LambdaQueryWrapper<StorageEntity> wrapper);
}
