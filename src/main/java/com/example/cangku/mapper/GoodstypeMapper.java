package com.example.cangku.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.cangku.entity.GoodstypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lihusheng
 * @since 2024-06-01
 */
@Mapper
public interface GoodstypeMapper extends BaseMapper<GoodstypeEntity> {
    IPage pageCC(IPage<GoodstypeEntity> page, @Param("ew") LambdaQueryWrapper<GoodstypeEntity> wrapper);

}
