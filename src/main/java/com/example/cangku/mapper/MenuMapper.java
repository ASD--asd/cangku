package com.example.cangku.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.cangku.entity.MenuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lihusheng
 * @since 2024-05-31
 */
@Mapper
public interface MenuMapper extends BaseMapper<MenuEntity> {

}
