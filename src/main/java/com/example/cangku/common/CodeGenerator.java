package com.example.cangku.common;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CodeGenerator {

    private static String url="jdbc:mysql://localhost:3306/wms02?useSSL=false&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";

    private static String username="root";

    private static String password="ASDasd123";

    private static String outputJavaDir = "D:\\spring\\demo\\cangku\\src\\main\\java";

    private static String outputXmlDir = "D:\\spring\\demo\\cangku\\src\\main\\resources\\mapper";

    public static void main(String[] args) {
        FastAutoGenerator.create( url, username, password )
                // 全局配置
                .globalConfig( (scanner, builder) -> builder
                        //.enableSwagger() // 开启 swagger 模式
                        .outputDir(outputJavaDir) // 指定输出目录
                        .author( scanner.apply( "请输入作者名称？" ) )
                        .dateType(DateType.TIME_PACK)//时间策略
                        .commentDate("yyyy-MM-dd")//注释日期
                        .fileOverride()
                )
                // 包配置
                .packageConfig( (scanner, builder) -> builder

                        .pathInfo( Collections.singletonMap( OutputFile.xml, outputXmlDir))
                        //.parent( scanner.apply( "请输入包名？" ) )
                        //.moduleName( scanner.apply( "请输入模块名？" ) )
                        .parent("com.example.demo")
                        .entity("entity")//Entity 包名
                        .service("service")//Service 包名
                        .serviceImpl("service.impl")//Service Impl 包名
                        .mapper("mapper")//Mapper 包名
                        .xml("mapper")//Mapper XML 包名
                        .controller("controller")//Controller 包名
                )

                // 策略配置
                .strategyConfig( (scanner, builder) -> builder
                                .addInclude( getTables( scanner.apply( "请输入表名，多个英文逗号分隔？所有输入 all" ) ) )
                                .addTablePrefix( "","","","t_" ) //设置过滤表前缀
                                .controllerBuilder()

                                .enableRestStyle()
                                .enableHyphenStyle()
                                .entityBuilder()
                                .enableLombok() //设置是否使用Lombok
                                .idType(IdType.AUTO)//全局主键类型
//                        .addTableFills(new Column("create_time", FieldFill.INSERT))//表字段填充
//                        .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))//表字段填充
                                .addTableFills(
                                        new Column( "create_time", FieldFill.INSERT ),
                                        new Column( "update_time", FieldFill.INSERT_UPDATE )
                                )
                                .formatFileName("%sEntity")//格式化文件名称
                                //Controller策略配置
                                .controllerBuilder()
                                .enableHyphenStyle()//开启驼峰转连字符 默认为false
                                .enableRestStyle()//开启生成@RestController 控制器 默认为false
                                .formatFileName("%sController")//格式化文件名称
                                //Service配置策略
                                .serviceBuilder()
                                .superServiceClass(IService.class)//设置 service 接口父类
                                .superServiceImplClass(ServiceImpl.class)//设置 service 实现类父类
                                .formatServiceFileName("%sService")//格式化 service 接口文件名称
                                .formatServiceImplFileName("%sServiceImpl")//格式化 service 实现类文件名称
                                //Mapper策略配置
                                .mapperBuilder()
                                .superClass(BaseMapper.class)//设置父类
                                .enableMapperAnnotation()//开启@Mapper注解 默认false
                                .enableBaseResultMap()//启用 BaseResultMap 生成  默认false
                                .enableBaseColumnList()//启用 BaseColumnList  默认false
//                .cache(MyMapperCache.class)//设置缓存实现类
                                .formatMapperFileName("%sMapper")//格式化 mapper 文件名称
                                .formatXmlFileName("%sMapper")//格式化 xml 实现类文件名称
                                .build()
                )
                /*
                    模板引擎配置，默认 Velocity 可选模板引擎 Beetl 或 Freemarker
                   .templateEngine(new BeetlTemplateEngine())*/
                .templateEngine(new FreemarkerTemplateEngine())

                .execute();
    }
    // 处理 all 情况
    protected static List<String> getTables (String tables){
        return "all".equals( tables ) ? Collections.emptyList() : Arrays.asList( tables.split( "," ) );
    }
}