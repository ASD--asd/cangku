package com.example.cangku.common;

import lombok.Data;

import java.util.HashMap;

@Data
public class QueryPageParam {
    private int pageSize;
    private int pageNum;
    
    private static int PAGE_SIZE=20;
    private static int PAGE_NUM=1;

    private HashMap<String,Object> param = new HashMap<String, Object>();
}
