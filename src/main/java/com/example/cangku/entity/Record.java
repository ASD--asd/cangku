package com.example.cangku.entity;

import lombok.Data;

@Data
public class Record extends RecordEntity{
    private String username;
    private String adminname;
    private String goodsname;
    private String storagename;
    private String goodstypename;

}
